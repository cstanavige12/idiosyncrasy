# README #

This is the repository my individual project for the CS461/CS462 Senior Project 2017 at Western Oregon University. This project is part of what is used as a capstone project for a Bachelor's degree in Computer Science.

## Repository Owner ##

* Cameron Stanavige

### Repository Contents ###

Information about the structure of this repository coming soon...

## Project ##

Information about my specific project coming soon...

### Guidlines ###

Coding and contributing guidlines for this project coming soon...

### Tools, Configurations, and Packages ###

A list of all the software, libraries, tools, packages, and versions used on this project coming soon...