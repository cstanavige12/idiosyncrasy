namespace Idiosyncrasy.DAL
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;
    using Idiosyncrasy.Models;

    public partial class Idiosyncrasy_Context : DbContext
    {
        public Idiosyncrasy_Context()
            : base("name=Idiosyncrasy_Context")
        {
        }

        public virtual DbSet<Bridge> Bridges { get; set; }
        public virtual DbSet<Card> Cards { get; set; }
        public virtual DbSet<Deck> Decks { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Card>()
                .HasMany(e => e.Bridges)
                .WithRequired(e => e.Card)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Deck>()
                .HasMany(e => e.Bridges)
                .WithRequired(e => e.Deck)
                .WillCascadeOnDelete(false);
        }
    }
}
