﻿using Idiosyncrasy.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Idiosyncrasy.Controllers
{
    public class DecksController : Controller
    {
        private Idiosyncrasy_Context db = new Idiosyncrasy_Context();

        // GET: Deck
        public ActionResult Index()
        {
            return RedirectToAction("All");
        }

        public JsonResult All()
        {
            db.Configuration.ProxyCreationEnabled = false;
            return Json(db.Decks, JsonRequestBehavior.AllowGet);
        }
    }
}