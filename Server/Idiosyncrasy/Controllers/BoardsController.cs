﻿using Idiosyncrasy.DAL;
using Idiosyncrasy.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Idiosyncrasy.Controllers
{
    public class BoardsController : Controller
    {
        //private const double PERCENT = 0.7;
        //private const int BOARDSIZE = 24;
        private Card FREE = new Card { ID = 0, Title = "FREE", Description = "Someone that is hiding something", Played = 0, Found = 0, Bridges = null };
        private Idiosyncrasy_Context db = new Idiosyncrasy_Context();

        // GET: Boards
        public ActionResult Index()
        {
            return RedirectToAction("Create", new { id = "1" });
        }

        public JsonResult Create(string id)
        {
            // get the ID's of the decks to pull cards from
            int[] decks = getDeckIDs(id);

            db.Configuration.ProxyCreationEnabled = false;
           
            /*
             24 cards taken from a random ordering of all the cards in any of the decks passed in
             */
            var cards = db.Cards.Where(c => c.Bridges.Any(b => decks.Contains(b.DeckID))).OrderBy(r => Guid.NewGuid()).Take(24).ToList();
            cards.Insert(12, FREE);

            return Json(cards, JsonRequestBehavior.AllowGet);
        }

        private int[] getDeckIDs(string id)
        {
            if (id == "" || id == null)
                id = "1";

            string[] ids = id.Split(',');
            HashSet<int> decks = new HashSet<int>();
            for (int i = 0; i < ids.Count(); i++)
            {
                int x;
                Int32.TryParse(ids[i], out x);
                if (x > 0 && x < 11)
                    decks.Add(x);
            }
            decks.Add(1);

            return decks.ToArray();
        }
    }
}