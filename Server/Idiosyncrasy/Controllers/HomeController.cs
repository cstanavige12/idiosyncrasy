﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Idiosyncrasy.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return RedirectToAction("Hello");
        }

        public JsonResult Hello()
        {
            var data = new { Message = "Hello World!",
                             IP = Request.UserHostAddress
                           };

            return Json(data, JsonRequestBehavior.AllowGet);
        }
    }
}