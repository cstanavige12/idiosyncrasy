-- *********** Drop Tables ***********
-- DONT RUN THIS SECTION WHEN RUNNING SCRIPT THE FIRST TIME

IF OBJECT_ID('dbo.Bridges','U') IS NOT NULL
	DROP TABLE [dbo].[Bridges];
GO

IF OBJECT_ID('dbo.Cards','U') IS NOT NULL
	DROP TABLE [dbo].[Cards];
GO

IF OBJECT_ID('dbo.Decks','U') IS NOT NULL
	DROP TABLE [dbo].[Decks];
GO

-- ########### Decks ###########
CREATE TABLE [dbo].[Decks]
(
	[ID] INT IDENTITY (1,1) NOT NULL,
	[Name] NVARCHAR (25) NOT NULL,
	[Description] NVARCHAR (250) NOT NULL,
	[Played] INT NOT NULL,
	CONSTRAINT [PK_dbo.Decks] PRIMARY KEY CLUSTERED ([ID] ASC)
);

-- ########### Cards ###########
CREATE TABLE [dbo].[Cards]
(
	[ID] INT IDENTITY (1,1) NOT NULL,
	[Title] NVARCHAR (50) NOT NULL,
	[Description] NVARCHAR (250) NOT NULL,
	[Played] INT NOT NULL,
	[Found] INT NOT NULL,
	CONSTRAINT [PK_dbo.Cards] PRIMARY KEY CLUSTERED ([ID] ASC)
);

-- ########### Bridges ###########
CREATE TABLE [dbo].[Bridges]
(
	[ID] INT IDENTITY (1,1) NOT NULL,
	[DeckID] INT NOT NULL,
	[CardID] INT NOT NULL,
	CONSTRAINT [PK_dbo.Bridges] PRIMARY KEY CLUSTERED ([ID] ASC),
	CONSTRAINT [FK_dbo.Bridges_dbo.DeckID] FOREIGN KEY ([DeckID]) REFERENCES [dbo].[Decks] ([ID]),
	CONSTRAINT [FK_dbo.Bridges_dbo.CardID] FOREIGN KEY ([CardID]) REFERENCES [dbo].[Cards] ([ID])
);