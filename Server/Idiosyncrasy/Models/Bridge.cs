namespace Idiosyncrasy.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class Bridge
    {
        public int ID { get; set; }

        public int DeckID { get; set; }

        public int CardID { get; set; }

        public virtual Card Card { get; set; }

        public virtual Deck Deck { get; set; }
    }
}
