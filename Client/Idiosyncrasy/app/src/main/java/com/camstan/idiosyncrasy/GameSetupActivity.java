package com.camstan.idiosyncrasy;

import android.app.ActionBar;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class GameSetupActivity extends AppCompatActivity {

    LinearLayout listDecks;
    //String url = "http://idiosyncrasy.azurewebsites.net/Home/Hello";
    String decksUrl = "http://idiosyncrasy.azurewebsites.net/Decks/All";
    List decksList = new ArrayList();
    RequestQueue requestQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_setup);

        requestQueue = Volley.newRequestQueue(this);

        listDecks = (LinearLayout) findViewById(R.id.decksLayout);

        JsonArrayRequest obreq = new JsonArrayRequest(Request.Method.GET, decksUrl, null,
            new Response.Listener<JSONArray>() {

                @Override
                public void onResponse(JSONArray response) {
                    try {
                        for(int i = 0; i < response.length(); i++) {
                            CheckBox cb = new CheckBox(getApplicationContext());
                            JSONObject deck = response.getJSONObject(i);
                            cb.setId(deck.getInt("ID"));
                            cb.setText(deck.getString("Name"));
                            final String desc = deck.getString("Description");
                            cb.setLayoutParams(new LinearLayout.LayoutParams
                                    (LinearLayout.LayoutParams.MATCH_PARENT,
                                            LinearLayout.LayoutParams.WRAP_CONTENT));
                            cb.setTextSize(TypedValue.COMPLEX_UNIT_SP, 24);
                            cb.setTextColor(Color.WHITE);
                            if (i == 0) {
                                cb.setChecked(true);
                                cb.setEnabled(false);
                                decksList.add(Integer.toString(cb.getId()));
                            }

                            listDecks.addView(cb);

                            if (i != 0) {
                                cb.setOnClickListener(new View.OnClickListener() {
                                    public void onClick(View v) {
                                        String chk = null;
                                        chk = Integer.toString(v.getId());
                                        if (((CheckBox) v).isChecked()) {
                                            decksList.add(chk);
                                        } else {
                                            decksList.remove(chk);
                                        }
                                    }
                                });

                                cb.setOnLongClickListener(new View.OnLongClickListener() {
                                    public boolean onLongClick(View v) {
                                        Toast.makeText(getApplicationContext(), desc, Toast.LENGTH_LONG).show();
                                        return true;
                                    }
                                });
                            }
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            },
            new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.e("Volley", "Error");
                }
            });
        requestQueue.add(obreq);
    }

    public void makeBoard(View view) {
        String boardUrl = "http://idiosyncrasy.azurewebsites.net/Boards/Create?id=";
        String param = "";
        for(int i = 0; i < decksList.size(); i++){
            if(i == decksList.size()-1)
                param += decksList.get(i);
            else
                param += decksList.get(i) + ",";
        }
        boardUrl += param;
        Intent intent = new Intent (this, GameBoardActivity.class);
        intent.putExtra("BoardUrl", boardUrl);
        startActivity(intent);
    }

/*    public void sayHello(View view) {
        Intent intent = new Intent(this, GameBoardActivity.class);

       requestQueue = Volley.newRequestQueue(this);

        JsonObjectRequest obreq = new JsonObjectRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            data += "Message: " + response.getString("Message");
                            data += "\nIP: " + response.getString("IP");
                        }
                        catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("Volley", "Error");
                    }
                });
        requestQueue.add(obreq);

        intent.putExtra("URL", url);
        startActivity(intent);
    }*/
}
