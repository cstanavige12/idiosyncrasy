package com.camstan.idiosyncrasy;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class GameBoardActivity extends AppCompatActivity {

    LinearLayout bingoBoard;
    LinearLayout[] rows;
    String url;
    RequestQueue requestQueue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_game_board);

        Bundle bundle = getIntent().getExtras();
        url = bundle.getString("BoardUrl");

        generateBoard();
    }

    public void generateBoard() {
        requestQueue = Volley.newRequestQueue(this);

        bingoBoard = (LinearLayout) findViewById(R.id.bingoBoard);
        rows = new LinearLayout[bingoBoard.getChildCount()];
        for(int c = 0; c < bingoBoard.getChildCount(); c++) {
            rows[c] = (LinearLayout) bingoBoard.getChildAt(c);
        }

        JsonArrayRequest obreq = new JsonArrayRequest(Request.Method.GET, url, null,
                new Response.Listener<JSONArray>() {

                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            int count = 0;
                            int rowCount = 0;
                            while(count < response.length()) {
                                for (int i = 0; i < response.length()/5; i++) {
                                    final Button bn = (Button) rows[rowCount].getChildAt(i);
                                    JSONObject card = response.getJSONObject(count);
                                    final String title = card.getString("Title");
                                    bn.setText(title);
                                    final String description = card.getString("Description");

                                    //add long click listeners
                                    bn.setOnLongClickListener(new View.OnLongClickListener() {
                                        public boolean onLongClick(View v) {
                                            AlertDialog alertDialog = new AlertDialog.Builder
                                                    (GameBoardActivity.this).create();
                                            alertDialog.setTitle(title);

                                            TextView myView = new TextView(GameBoardActivity.this);
                                            myView.setText(description);
                                            myView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
                                            myView.setGravity(Gravity.CENTER_HORIZONTAL);
                                            alertDialog.setView(myView);

                                            //alertDialog.setMessage(description);
                                            alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                                                    new DialogInterface.OnClickListener() {
                                                        public void onClick(DialogInterface dialog, int which) {
                                                            dialog.dismiss();
                                                        }
                                                    });
                                            alertDialog.show();
                                            return true;
                                        }
                                    });

                                    count++;
                                }
                                rowCount++;
                            }
                        }
                        catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.e("Volley", "Error");
                    }
                });
        requestQueue.add(obreq);
    }

    public void onClick(View v) {
        switch(v.getId()) {
            case R.id.card0 :
            case R.id.card8 :
            case R.id.card11 :
            case R.id.card19 :
            case R.id.card22:
                if(v.isFocusable()){
                    v.setBackgroundColor(Color.parseColor("#E6E6E6"));
                    v.setFocusable(false);
                } else {
                    v.setBackgroundColor(Color.parseColor("#A0BF16"));
                    v.setFocusable(true);
                }
                break;
            case R.id.card1 :
            case R.id.card9 :
            case R.id.card12 :
            case R.id.card15 :
            case R.id.card23 :
                if(v.isFocusable()){
                    v.setBackgroundColor(Color.parseColor("#E6E6E6"));
                    v.setFocusable(false);
                } else {
                    v.setBackgroundColor(Color.parseColor("#14BDEB"));
                    v.setFocusable(true);
                }
                break;
            case R.id.card2 :
            case R.id.card5 :
            case R.id.card13 :
            case R.id.card16 :
            case R.id.card24 :
                if(v.isFocusable()){
                    v.setBackgroundColor(Color.parseColor("#E6E6E6"));
                    v.setFocusable(false);
                } else {
                    v.setBackgroundColor(Color.parseColor("#FF5964"));
                    v.setFocusable(true);
                }
                break;
            case R.id.card3 :
            case R.id.card6 :
            case R.id.card14 :
            case R.id.card17 :
            case R.id.card20 :
                if(v.isFocusable()){
                    v.setBackgroundColor(Color.parseColor("#E6E6E6"));
                    v.setFocusable(false);
                } else {
                    v.setBackgroundColor(Color.parseColor("#FFBC42"));
                    v.setFocusable(true);
                }
                break;
            case R.id.card4 :
            case R.id.card7 :
            case R.id.card10 :
            case R.id.card18 :
            case R.id.card21 :
                if(v.isFocusable()){
                    v.setBackgroundColor(Color.parseColor("#E6E6E6"));
                    v.setFocusable(false);
                } else {
                    v.setBackgroundColor(Color.parseColor("#38618C"));
                    v.setFocusable(true);
                }
                break;
            default :
                break;
        }
    }

    public void regenerateBoard(View view) {
        generateBoard();
    }

    public void startGame(View view) {
        AlertDialog alertDialog = new AlertDialog.Builder
                (GameBoardActivity.this).create();
        alertDialog.setTitle("Idiosyncrasy");

        TextView myView = new TextView(this);
        myView.setText("Good luck and have fun!");
        myView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
        myView.setGravity(Gravity.CENTER_HORIZONTAL);
        alertDialog.setView(myView);

        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.show();

        Button regen = (Button) findViewById(R.id.regenButton);
        regen.setEnabled(false);

        Button start = (Button) findViewById(R.id.startButton);
        start.setEnabled(false);
    }
}
